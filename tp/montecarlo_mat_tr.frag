
layout(location=20) uniform int nb_emissives;
layout(location=21) uniform int NB_BOUNCES;
layout(location=23) uniform int k_op;


vec3 sample_hemisphere(float rugosite)
{
	// Algo echantillonnage uniforme hemisphere:
	// Z <- rand entre ? et ?
	float z =rugosite+random_float()*(1.0-rugosite);
	//https://fr.wikipedia.org/wiki/Coordonn%C3%A9es_polaires#:~:text=Coordonn%C3%A9es%20sph%C3%A9riques,-Un%20point%20rep%C3%A9r%C3%A9&text=En%20math%C3%A9matiques%2C%20en%20nommant%20les,90%C2%B0%20et%2090%C2%B0).
	// beta <- Z : angle/plan_xy
	float phi = acos(z);
	// alpha <- rand entre ? et ?
	float teta = random_float()*2.0*PI;
	// x,y,z <- alpha,beta : coord polaire -> cartesienne
	float x = sin(phi)*cos(teta);
	float y = sin(phi)*sin(teta);
	// fake !!!!!!
	return normalize(vec3(x,y,z));
}

// D direction principale de l'hemisphere, normalisée
vec3 random_ray(in vec3 D,float rugosite)
{
	// Algo orientation échantillon
	// choisir un W normalisé non colineaire à D
	vec3 W = normalize(vec3(D.z,D.x, -D.y));
	//vec3 W = normalize(vec3(D.z,D.x,D.y));
	// U orthogonal à D et W
	vec3 U = normalize(cross(D,W));
	// V tq U,V,D repère ortho-normé direct
	vec3 V = normalize(cross(U,D));
	// mettre  U,V,D dans une  matrice 3x3 de changement de repère M
	mat3 M = mat3(U,V,D);
	// multiplier votre echantillon par M pour bien l'orienter
	// ici par de matrice 4x4 car pas de translation
	return M*sample_hemisphere(rugosite);

}





vec3 random_path(in vec3 D, in vec3 O)
{
	float cost = 1.0;
	vec3 N;
	vec3 P;
	vec4 mat ;
	vec4 col = vec4(1.0) ;
	vec4 tmp_col = vec4(0);
	
	for(int i =0; (i<NB_BOUNCES+1);i++){
		
		traverse_all_bvh(O,D);
		
		//Si pas touché
		if (!hit())
			return col.rgb*vec3(0.75) ;

		//Sinon
		
		mat = intersection_mat_info();
		tmp_col = intersection_color_info();
		
		
		
		if(mat.b>0)
			return col.rgb *mat.b;
		
		
		intersection_info(N,P);
		
		bool doAlpha = random_float() >tmp_col.a;
		if(doAlpha){
				O = P-N*0.001;
				D = refract(D,N,1/1.25);
				traverse_all_bvh(O,D);
				if (!hit())
					return (col *mix(vec4(0.75) ,col,tmp_col.a)).rgb;
				intersection_info(N,P);
				O = P+N*0.001;
				D = refract(D,-N,1.25);
				traverse_all_bvh(O,D);
				tmp_col = intersection_color_info();
				col*= mix(tmp_col,col,tmp_col.a);
				intersection_info(N,P);
				O = P+N*0.001;

		}else{
			
			O = P+N*0.001;

			if(mat.r>0){

					col *=mix(tmp_col,vec4(1.0),mat.r);
					D = random_ray(reflect(D,normalize(N)),mat.g);
					cost = mat.r;
			}
			else{
				
					col *=tmp_col*cost;
					D = random_ray(normalize(N),mat.g);
					cost = max(0.0,dot(D,N));
				
		}
		}
	}

	return vec3(0.0);
}

vec3 raytrace(in vec3 Dir, in vec3 Orig)   
{
	// init de la graine du random
	srand();
	// calcul de la lumière captée par un chemin aléatoire
	return random_path(normalize(Dir),Orig);
}

