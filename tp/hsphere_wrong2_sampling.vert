
vec3 sample_hemisphere(float rugosite)
{
	// Algo echantillonnage uniforme hemisphere:
	// Z <- rand entre ? et ?
	float z =rugosite+random_float()*(1.0-rugosite);
	//https://fr.wikipedia.org/wiki/Coordonn%C3%A9es_polaires#:~:text=Coordonn%C3%A9es%20sph%C3%A9riques,-Un%20point%20rep%C3%A9r%C3%A9&text=En%20math%C3%A9matiques%2C%20en%20nommant%20les,90%C2%B0%20et%2090%C2%B0).
	// beta <- Z : angle/plan_xy
	float phi = acos(z);
	// alpha <- rand entre ? et ?
	float teta = random_float()*2.0*PI;
	// x,y,z <- alpha,beta : coord polaire -> cartesienne
	float x = sin(phi)*cos(teta);
	float y = sin(phi)*sin(teta);
	// fake !!!!!!
	return normalize(vec3(x,y,z));
}

// D direction principale de l'hemisphere, normalisée
vec3 random_ray(in vec3 D,float rugosite)
{
	// Algo orientation échantillon
	// choisir un W normalisé non colineaire à D
	vec3 W = normalize(vec3(D.z,D.x, -D.y));
	//vec3 W = normalize(vec3(D.z,D.x,D.y));
	// U orthogonal à D et W
	vec3 U = normalize(cross(D,W));
	// V tq U,V,D repère ortho-normé direct
	vec3 V = normalize(cross(U,D));
	// mettre  U,V,D dans une  matrice 3x3 de changement de repère M
	mat3 M = mat3(U,V,D);
	// multiplier votre echantillon par M pour bien l'orienter
	// ici par de matrice 4x4 car pas de translation
	return M*sample_hemisphere(rugosite);

}


void main()
{
	// param de srand le nombre de random_float appelé dans le shader
	srand(3u);
	vec3 P = random_ray(normalize(normal),param);
	gl_Position = pvMatrix * vec4(P,1);
}