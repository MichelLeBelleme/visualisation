
layout(location=20) uniform int nb_emissives;
layout(location=21) uniform int NB_BOUNCES;


vec3 sample_hemisphere()
{
	float z = random_float();
	float phi = acos(z);
	float teta = random_float()*2.0*PI;
	float x = sin(phi)*cos(teta);
	float y = sin(phi)*sin(teta);
	return normalize(vec3(x,y,z));
}

// D direction principale de l'hemisphere, normalisée
vec3 random_ray(in vec3 D)
{

	vec3 W = normalize(vec3(D.z,D.x, -D.y));
	vec3 U = normalize(cross(D,W));
	vec3 V = normalize(cross(U,D));
	mat3 M = mat3(U,V,D);
	return M*sample_hemisphere();
}





vec3 random_path(in vec3 D, in vec3 O)
{

	float cost = 1.0;
	vec3 N;
	vec3 P;
	vec4 mat ;
	vec4 col = vec4(1.0) ;
	vec4 tmp_col = vec4(0);
	
	for(int i =0; (i<NB_BOUNCES+1) ;i++){
		
		traverse_all_bvh(O,D);
		
		//Si pas touché
		if (!hit())
			return col.rgb*vec3(25.0/255.0,25.0/255.0,125.0/255.0) ;

		//Sinon
		
		mat = intersection_mat_info();
		tmp_col = intersection_color_info();
		
		
		
		if(mat.b>0)
			return col.rgb *mat.b ;
		col *=tmp_col *cost;
		
		intersection_info(N,P);
		O = P+N*0.001;
		D = random_ray(N);
		cost = max(0.0,dot(D,N));

		
	}

	return vec3(0.0);
}


vec3 raytrace(in vec3 Dir, in vec3 Orig)   
{
	// init de la graine du random
	srand();
	// calcul de la lumière captée par un chemin aléatoire
	return random_path(normalize(Dir),Orig);
}

